# PDM
Docker image with [PDM](https://pdm-project.org/en/latest/) installed.

# Usage

[Woodpecker](https://woodpecker-ci.org/)  
```
steps:
  - name: example
    image: registry.gitlab.com/thiagola92/pdm:2.13.3
    commands:
        - pdm version
```

# Development
Edit Dockerfile and update registry images.  

Latest  
```
sudo docker build -t registry.gitlab.com/thiagola92/pdm:latest .
sudo docker push registry.gitlab.com/thiagola92/pdm:latest
```

Specific version:  
```
sudo docker build -t registry.gitlab.com/thiagola92/pdm:2.13.3 .
sudo docker push registry.gitlab.com/thiagola92/pdm:2.13.3
```