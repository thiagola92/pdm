FROM python:3.12.2-bookworm

# Install PDM.
RUN curl -sSL https://pdm-project.org/install-pdm.py | python3 -

# So following commands find PDM executable.
RUN cp ~/.local/bin/pdm /usr/local/bin